﻿using System;
using POSService;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PointOfSaleTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void OrderItemTest_1()
        {

            var terminal = new PointOfSaleService();
            terminal.SetPricing();

            terminal.ScanProduct("A");
            terminal.ScanProduct("B");
            terminal.ScanProduct("C");
            terminal.ScanProduct("D");
            terminal.ScanProduct("A");
            terminal.ScanProduct("B");
            terminal.ScanProduct("A");


            string expected = "$13.25";
            string actual = terminal.CalculateTotal();

            Assert.AreEqual(expected, actual, "Invoice amount is incorrect");

        }

        [TestMethod]
        public void OrderItemTest_2()
        {

            var terminal = new PointOfSaleService();
            terminal.SetPricing();

            terminal.ScanProduct("A");
            terminal.ScanProduct("B");
            terminal.ScanProduct("C");
            terminal.ScanProduct("D");


            string expected = "$7.25";
            string actual = terminal.CalculateTotal();

            Assert.AreEqual(expected, actual, "Invoice amount is incorrect");
        }

        [TestMethod]
        public void OrderItemTest_3()
        {
            var terminal = new PointOfSaleService();
            terminal.SetPricing();

            terminal.ScanProduct("C");
            terminal.ScanProduct("C");
            terminal.ScanProduct("C");
            terminal.ScanProduct("C");
            terminal.ScanProduct("C");
            terminal.ScanProduct("C");
            terminal.ScanProduct("C");


            string expected = "$6.00";
            string actual = terminal.CalculateTotal();

            Assert.AreEqual(expected, actual, "Invoice amount is incorrect");
        }
    }
}
