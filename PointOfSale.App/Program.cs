﻿using POSService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSTerminalApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PointOfSaleService pOS = new PointOfSaleService();
            pOS.SetPricing();

            Console.WriteLine("*** POS Terminal App ***");
            Console.WriteLine("Start by scanning your 1st Product");
            bool calculateTotal = false;

            do
            {
                string productCode;

                // Get Product code input
                productCode = Console.ReadLine();

                if (productCode.Equals("0"))
                {
                    calculateTotal = true;
                }
                else
                {

                    pOS.ScanProduct(productCode);

                    // Get Product code input
                    Console.WriteLine("Scan another Product or press 0 to Calculate Total & Pay");

                }
            } while (!calculateTotal);

            string result = pOS.CalculateTotal();

            Console.WriteLine("Current Total " + result);
            Console.ReadLine();
            Console.WriteLine("Exiting App");

        }
    }
}
