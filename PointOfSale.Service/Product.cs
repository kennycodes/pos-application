﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSService
{
    class Product
    {
        public string productCode { get; set; }


        public decimal retailPrice { get; set; }

        public int bulkQuantity { get; set; }

        public decimal bulkPrice { get; set; }


    }
}
