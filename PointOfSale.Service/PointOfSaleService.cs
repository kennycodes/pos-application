﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace POSService
{
    public class PointOfSaleService
    {
        IDictionary<string, int> _shoppingCart;
        List<Product> _inventory;

        public PointOfSaleService() {
            _shoppingCart = new Dictionary<string, int>();
            SetPricing();
        }

        public void SetPricing() {

            using (StreamReader r = new StreamReader("..\\..\\inputData.json"))
            {
                string json = r.ReadToEnd();
                _inventory = JsonConvert.DeserializeObject<List<Product>>(json);
            }

        }

        public void ScanProduct(string productCode)
        {
            // Assumption - productCode is case in-sensitive 
            var code = productCode.ToUpper();

            // Assumption - productCode cannot be blank
            if (string.IsNullOrWhiteSpace(productCode))
            {
                return;
            }
            
            if (_shoppingCart.ContainsKey(code))
            {
                _shoppingCart[code]++;
            }
            else
            {
                _shoppingCart.Add(code, 1);
            }
        }

        public string CalculateTotal()
        {
            decimal grandTotal = 0;
            foreach( var item in _shoppingCart)
            {
                decimal itemTotal = 0;

                if(_inventory.Exists(x => x.productCode == item.Key) && item.Value > 0)
                {
                    Product product = _inventory.Find(x => x.productCode == item.Key);

                    int bulkPriceItems = 0;

                    // Check for and calculate the Bulk Item cost
                    if (item.Value >= product.bulkQuantity && product.bulkQuantity > 0)
                    {
                        bulkPriceItems = item.Value / product.bulkQuantity;

                        if (bulkPriceItems > 0)
                        {
                            itemTotal = bulkPriceItems * product.bulkPrice;
                        }
                    }

                    // Calculate the Retail Item cost
                    int retailItems = item.Value - (bulkPriceItems * product.bulkQuantity);

                    itemTotal = itemTotal + (retailItems * product.retailPrice);

                }

                grandTotal += itemTotal;
            }

            // Formatted the total to show the dollar amount here - although formatting can be done as separate helper class
            return grandTotal.ToString("$0.00");
        }



    }
}
